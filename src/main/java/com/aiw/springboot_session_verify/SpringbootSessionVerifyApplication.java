package com.aiw.springboot_session_verify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@ServletComponentScan
@SpringBootApplication
public class SpringbootSessionVerifyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSessionVerifyApplication.class, args);
    }

}
