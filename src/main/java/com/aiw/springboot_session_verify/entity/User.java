package com.aiw.springboot_session_verify.entity;

import lombok.Data;

@Data
public class User {
    private Integer id;

    private String name;
}
