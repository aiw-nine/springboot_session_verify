//package com.aiw.springboot_session_verify.filter;
//
//import com.aiw.springboot_session_verify.response.R;
//import com.alibaba.fastjson.JSON;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.util.AntPathMatcher;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.Objects;
//
///**
// * 检查用户是否已经完成登录（方式一：过滤器）
// * 需要在启动类上加上@ServletComponentScan注解，这样才会扫描@WebFilter注解
// */
//@Slf4j
//@WebFilter
//public class LoginCheckFilter implements Filter {
//    // 路径匹配器，支持通配符
//    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        log.info("拦截到的请求：{}", request.getRequestURI());
//        // 1、获取本次请求的URI
//        String requestURI = request.getRequestURI();
//        // 定义不需要处理的请求路径
//        String[] urls = new String[]{"/user/login", "/user/logout"};
//
//        // 2、判断本次请求是否需要处理
//        boolean check = check(urls, requestURI);
//
//        // 3、如果不需要处理，则直接放行
//        if (check) {
//            log.info("本次请求{}不需要处理", requestURI);
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        // 4、判断登录状态，如果已登录，则直接放行
//        if (Objects.nonNull(request.getSession().getAttribute("user"))) {
//            log.info("用户已登录，用户id为：{}", request.getSession().getAttribute("user"));
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        // 5、如果未登录则返回未登录结果，通过输出流方式向客户端页面响应数据
//        log.info("用户未登录");
//        response.setContentType("application/json; charset=utf-8");
//        // 1、使用Fastjson（默认过滤null值）
//        response.getWriter().write(JSON.toJSONString(R.error("未登录")));
//        // 2、使用默认的Jackson，此处关于Jackson配置的相关属性会失效（即若在配置文件中配置过滤null值，这里返回时不会过滤）
//        // response.getWriter().write(new ObjectMapper().writeValueAsString(R.error("未登录")));
//        return;
//    }
//
//    /**
//     * 路径匹配，检查本次请求是否需要放行
//     *
//     * @param urls
//     * @param requestURI
//     * @return
//     */
//    public boolean check(String[] urls, String requestURI) {
//        for (String url : urls) {
//            boolean match = PATH_MATCHER.match(url, requestURI);
//            if (match) return true;
//        }
//        return false;
//    }
//}
