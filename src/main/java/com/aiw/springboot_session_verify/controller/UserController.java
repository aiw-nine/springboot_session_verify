package com.aiw.springboot_session_verify.controller;

import com.aiw.springboot_session_verify.entity.User;
import com.aiw.springboot_session_verify.response.R;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 登录，此处只做简单测试
     *
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public R<User> login(@RequestBody User user, HttpServletRequest request) {
        // 此处应该和数据库进行交互判断，为做测试，简单写死
        if (Objects.equals(user.getId(), 1) && Objects.equals(user.getName(), "Aiw")) {
            // 登录成功，将id存入session并返回登录成功结果
            request.getSession().setAttribute("user", user.getId());
            request.getSession().setMaxInactiveInterval(1800);  // 设置session失效时间为30分钟
            return R.success("登录成功", user);
        }
        return R.fail("登录失败");
    }

    /**
     * 退出登录
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public R<String> logout(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        return R.success("退出成功");
    }

    /**
     * 此处做测试，看用户在未登录时，能否访问到此接口
     *
     * @return
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public R<String> index() {
        return R.success("首页，访问成功");
    }

}
