package com.aiw.springboot_session_verify.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用返回结果，服务端响应的数据最终都会封装成此对象
 *
 * @param <T> 泛型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class R<T> {

    private Integer code; //编码：200成功，400失败，403禁止请求（无权限，需要登录）

    private String msg; //错误信息

    private T data; //数据

    public static <T> R<T> success(String msg) {
        return success(msg, null);
    }

    public static <T> R<T> success(String msg, T object) {
        return new R<>(200, msg, object);
    }

    public static <T> R<T> fail(String msg) {
        return new R<>(400, msg, null);
    }

    public static <T> R<T> error(String msg) {
        return new R<>(403, msg, null);
    }

}
